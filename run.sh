source tarkov.env

docker run --rm -ti \
  -e "HWID=${HWID}" \
  -e "TARKOV_EMAIL=${TARKOV_EMAIL}" \
  -e "TARKOV_PASSWORD=${TARKOV_PASSWORD}" \
  -e "STATSD_HOST=${STATSD_HOST}" \
  ramielrowe/tarkov-monitor:latest