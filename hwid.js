const console = require('console');
const crypto = require('crypto');

const random_hash = () => {
  let hash = crypto.createHash('sha1').update(Math.random().toString()).digest('hex');
  return hash;
}

const short_hash = () => {
  let hash = random_hash();
  return hash.substring(0, hash.length - 8);
}

const h = `#1-${random_hash()}:${random_hash()}:${random_hash()}-${random_hash()}-${random_hash()}-${random_hash()}-${random_hash()}-${short_hash()}`;

console.log(h)
