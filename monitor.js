
const { Tarkov } = require('tarkov');
const SDC = require('statsd-client');

const MARKET_SEARCH_RUBLES = 1;
const MARKET_SEARCH_DOLLARS = 2;
const MARKET_SEARCH_EUROS = 3;

const ROUBLE_ID = '5449016a4bdc2d6f028b456f';
const DOLLAR_ID = '5696686a4bdc2da3298b456a';
const EURO_ID = '569668774bdc2da2298b4568';

const ITEMS = {
  zarya: {
    name: 'zarya',
    id: '5a0c27731526d80618476ac4',
  },
  salewa: {
    name: 'salewa',
    id: '544fb45d4bdc2dee738b4568',
  },
  paracord: {
    name: 'paracord',
    id: '5c12688486f77426843c7d32',
  },
  dfuel: {
    name: 'dfuel',
    id: '590a373286f774287540368b',
  },
  fuel_cond: {
    name: 'fuel_cond',
    id: '5b43575a86f77424f443fe62',
  }
};

const CURRENCY = {
  rubles: {
    name: 'rubles',
    search: MARKET_SEARCH_RUBLES,
    id: ROUBLE_ID,
  },
  dollars: {
    name: 'dollars',
    search: MARKET_SEARCH_DOLLARS,
    id: DOLLAR_ID,
  },
  euros: {
    name: 'euros',
    search: MARKET_SEARCH_EUROS,
    id: EURO_ID,
  },
};

const SEARCHES = [
  {
    item: ITEMS.zarya,
    currency: CURRENCY.rubles,
  },
  {
    item: ITEMS.salewa,
    currency: CURRENCY.rubles,
  },
  {
    item: ITEMS.paracord,
    currency: CURRENCY.rubles,
  },
  {
    item: ITEMS.dfuel,
    currency: CURRENCY.rubles,
  },
  {
    item: ITEMS.fuel_cond,
    currency: CURRENCY.rubles,
  },
];

const hwid = process.env.HWID;
const email = process.env.TARKOV_EMAIL;
const password = process.env.TARKOV_PASSWORD;
const statsdHost = process.env.STATSD_HOST;

const sdc = new SDC({host: statsdHost});
var t = new Tarkov(hwid);

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
};

function randRange(min, max) {
  return Math.floor(Math.random() * (max - min) ) + min;
}

async function runSearch(searchDef) {
  const search = await t.searchMarket(0, 50, {
    sortType: 5,
    sortDirection: 0,
    currency: searchDef.currency.search,
    handbookId: searchDef.item.id,
  });

  if (search.offers.length > 0) {
    var tags = {
      'item': searchDef.item.name
    };
    const low = search.offers[0].requirementsCost;

    var volume = 0;
    var totalCost = 0;
    var weightedVolume = 0;
    var weightedTotalCost = 0;

    search.offers.forEach((element) => {
      volume += 1;
      totalCost += element.requirementsCost
      const offerCount = element.items[0].upd.StackObjectsCount;
      weightedVolume += offerCount;
      weightedTotalCost += offerCount * element.requirementsCost;
    });

    var average = totalCost / volume;
    var weightedAverage = weightedTotalCost / weightedVolume;

    //console.log(`(${searchDef.item.name}) volume: ${volume}`)
    //console.log(`(${searchDef.item.name}) average: ${average}`)
    //console.log(`(${searchDef.item.name}) weightedVolume: ${weightedVolume}`)
    //console.log(`(${searchDef.item.name}) weightedAverage: ${weightedAverage}`)

    sdc.gauge('tarkov.market.low', low, tags=tags);
    sdc.gauge('tarkov.market.volume', volume, tags=tags);
    sdc.gauge('tarkov.market.average', average, tags=tags);
    sdc.gauge('tarkov.market.weighted.volume', weightedVolume, tags=tags);
    sdc.gauge('tarkov.market.weighted.average', weightedAverage, tags=tags);
    
  }
};

(async () => {
  while (true) {
    const start = (new Date()).getTime()

    await t.login(email, password);

    const profiles = await t.getProfiles();
    await t.selectProfile(profiles[0]);
  
    await t.getI18n('en');
  
    // jitter logins between 1 and 15 minutes
    const loginJitter = randRange(1, 15) * (1000 * 60)
    while (true) {
      for (let i = 0; i < SEARCHES.length; i++) {
        await runSearch(SEARCHES[i])
        await sleep(3000 + randRange(138, 1000))
      }
      // re-login every 3 hours
      if (start < ((new Date()).getTime() - (3 * 60 * 60 * 1000) - loginJitter)) {
        break
      }
    }
    // sleep 30 seconds between logins
    sleep(30*1000)
  }
  
  
})();
