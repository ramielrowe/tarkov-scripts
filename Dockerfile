from node:latest

RUN npm install --save-prod tarkov && npm install --save-prod statsd-client

ADD monitor.js /monitor.js

CMD ["node", "/monitor.js"]